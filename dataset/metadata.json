{
  "data_set_description": {
    "collection_date": "30-09-2022",
    "creator": "Ihsan Ullah",
    "default_target_attribute": "CATEGORY",
    "description": "## **Meta-Album Plant Doc Dataset (Mini)**\n***\nThe PlantDoc dataset(https://github.com/pratikkayal/PlantDoc-Dataset) is made up of images of leaves of healthy and unhealthy plants. The images were downloaded from Google Images and Ecosia, and later cropped by the authors, so generally, one complete leaf fits in one image. The original, uncropped images are generally different in scale, light conditions, and pose. However, within one category, images of leaves that came from the same original image can be found. The images correspond to 27 classes, including plant disease names and plant species names, e.g.: Corn Leaf Blight and Cherry Leaf respectively. The dataset was created for a benchmarking classification model work, published in 2020 by Singh et al. The PlantDoc dataset in the Meta-Album benchmark is extracted from a preprocessed version of the original PlantDoc dataset. First, to get i.i.d. samples, only one leaf image per each original image is randomly picked. Then, leaves images are cropped and made into squared images which are then resized into 128x128 with anti-aliasing filter.  \n\n\n\n### **Dataset Details**\n![](https://meta-album.github.io/assets/img/samples/PLT_DOC.png)\n\n**Meta Album ID**: PLT_DIS.PLT_DOC  \n**Meta Album URL**: [https://meta-album.github.io/datasets/PLT_DOC.html](https://meta-album.github.io/datasets/PLT_DOC.html)  \n**Domain ID**: PLT_DIS  \n**Domain Name**: Plant Diseases  \n**Dataset ID**: PLT_DOC  \n**Dataset Name**: Plant Doc  \n**Short Description**: Plant disease dataset  \n**\\# Classes**: 27  \n**\\# Images**: 1080  \n**Keywords**: plants, plant diseases,  \n**Data Format**: images  \n**Image size**: 128x128  \n\n**License (original data release)**: Creative Commons Attribution 4.0 International  \n**License URL(original data release)**: https://github.com/pratikkayal/PlantDoc-Object-Detection-Dataset/blob/master/LICENSE.txt\n \n**License (Meta-Album data release)**: Creative Commons Attribution 4.0 International  \n**License URL (Meta-Album data release)**: [https://creativecommons.org/licenses/by/4.0/](https://creativecommons.org/licenses/by/4.0/)  \n\n**Source**: PlantDoc: A Dataset for Visual Plant Disease Detection  \n**Source URL**: https://github.com/pratikkayal/PlantDoc-Object-Detection-Dataset  \n  \n**Original Author**: Sharada Mohanty, David Hughes, and Marcel Salathe  \n**Original contact**:   \n\n**Meta Album author**: Maria Belen Guaranda Cabezas  \n**Created Date**: 01 March 2022  \n**Contact Name**: Ihsan Ullah  \n**Contact Email**: meta-album@chalearn.org  \n**Contact URL**: [https://meta-album.github.io/](https://meta-album.github.io/)  \n\n\n\n### **Cite this dataset**\n```\n@inproceedings{10.1145/3371158.3371196,\n    author = {Singh, Davinder and Jain, Naman and Jain, Pranjali and Kayal, Pratik and Kumawat, Sudhakar and Batra, Nipun},\n    title = {PlantDoc: A Dataset for Visual Plant Disease Detection},\n    year = {2020},\n    isbn = {9781450377386},\n    publisher = {Association for Computing Machinery},\n    address = {New York, NY, USA},\n    url = {https://doi.org/10.1145/3371158.3371196},\n    doi = {10.1145/3371158.3371196},\n    booktitle = {Proceedings of the 7th ACM IKDD CoDS and 25th COMAD},\n    pages = {249-253},\n    numpages = {5},\n    keywords = {Object Detection, Image Classification, Deep Learning},\n    location = {Hyderabad, India},\n    series = {CoDS COMAD 2020}\n}\n```\n\n\n### **Cite Meta-Album**\n```\n@inproceedings{meta-album-2022,\n        title={Meta-Album: Multi-domain Meta-Dataset for Few-Shot Image Classification},\n        author={Ullah, Ihsan and Carrion, Dustin and Escalera, Sergio and Guyon, Isabelle M and Huisman, Mike and Mohr, Felix and van Rijn, Jan N and Sun, Haozhe and Vanschoren, Joaquin and Vu, Phan Anh},\n        booktitle={Thirty-sixth Conference on Neural Information Processing Systems Datasets and Benchmarks Track},\n        url = {https://meta-album.github.io/},\n        year = {2022}\n    }\n```\n\n\n### **More**\nFor more information on the Meta-Album dataset, please see the [[NeurIPS 2022 paper]](https://meta-album.github.io/paper/Meta-Album.pdf)  \nFor details on the dataset preprocessing, please see the [[supplementary materials]](https://openreview.net/attachment?id=70_Wx-dON3q&name=supplementary_material)  \nSupporting code can be found on our [[GitHub repo]](https://github.com/ihsaan-ullah/meta-album)  \nMeta-Album on Papers with Code [[Meta-Album]](https://paperswithcode.com/dataset/meta-album)  \n\n\n\n### **Other versions of this dataset**\n[[Micro]](https://www.openml.org/d/44273)  [[Extended]](https://www.openml.org/d/44336)",
    "description_version": "2",
    "file_id": "22111003",
    "format": "arff",
    "id": "44303",
    "language": "English",
    "licence": "CC BY-NC 4.0",
    "md5_checksum": "238ae5784b56f782c40aa771878f6d71",
    "minio_url": "https://data.openml.org/datasets/0004/44303/dataset_44303.pq",
    "name": "Meta_Album_PLT_DOC_Mini",
    "parquet_url": "https://data.openml.org/datasets/0004/44303/dataset_44303.pq",
    "processing_date": "2022-10-28 16:28:10",
    "status": "active",
    "tag": [
      "Agriculture",
      "Biology",
      "Data Science",
      "Image Processing",
      "Machine Learning"
    ],
    "upload_date": "2022-10-28T16:27:29",
    "url": "https://api.openml.org/data/v1/download/22111003/Meta_Album_PLT_DOC_Mini.arff",
    "version": "1",
    "visibility": "public"
  }
}